﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour
{
    public GameObject enemyPrefab;
    public float respawnRecharge = 5.0f;
   //public Vector22 screenBounds;
    // Start is called before the first frame update
    void Start()
    {
   //   screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.z));
    }

    private void spawnEnemy()
    {
        GameObject x = Instantiate(enemyPrefab) as GameObject; //add enemyhe scene
        //x.transform.position = new Vector2(screenBounds.x * -2, Random.Range(-screenbounds.y, screenBounds.y));
        StartCoroutine(enemyRespawnCooldown());

    }

    IEnumerator enemyRespawnCooldown()
    {
        while (true)
        {
            yield return new WaitForSeconds(respawnRecharge);
            spawnEnemy();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
