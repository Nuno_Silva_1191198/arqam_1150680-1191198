﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Canvas_Manager: MonoBehaviour
{
    public GameObject main_menu_canvas;
    public GameObject settings_canvas;
    public GameObject credits_canvas;
    public GameObject wiki_canvas;
    public ParticleSystem particle_system;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Change_To_Canvas(int canvas_ID)
    {
        switch (canvas_ID)
        {
            case 0:
                {
                    main_menu_canvas.SetActive(true);
                    settings_canvas.SetActive(false);
                    credits_canvas.SetActive(false);
                    wiki_canvas.SetActive(false);
                }
                break;

            case 1:
                {
                    main_menu_canvas.SetActive(false);
                    settings_canvas.SetActive(true);
                    credits_canvas.SetActive(false);
                    wiki_canvas.SetActive(false);
                }
                break;

            case 2:
                {
                    main_menu_canvas.SetActive(false);
                    settings_canvas.SetActive(false);
                    credits_canvas.SetActive(true);
                    wiki_canvas.SetActive(false);
                }
                break;

            case 3:
                {
                    main_menu_canvas.SetActive(false);
                    settings_canvas.SetActive(false);
                    credits_canvas.SetActive(false);
                    wiki_canvas.SetActive(true);
                }
                break;

            default:
                {
                    Debug.Log("Bom Dia");
                }
                break;
        }
    }

    public void Change_To_Level(int level_ID)
    {
        SceneManager.LoadSceneAsync(level_ID);
    }
}
