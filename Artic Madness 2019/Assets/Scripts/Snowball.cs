﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snowball : MonoBehaviour
{
    //firepoint?
    public static Transform firepoint;
    public Rigidbody2D rb;
    public GameObject snowballPrefab;  //object to create
    public static int damage = 25;
    public float speed = 20f;

    Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
    Vector2 firepointPosition = new Vector2(firepoint.position.x, firepoint.position.y);
    //se der update ok, se nao criar objecto again la em baixo


    public 

    // Start is called before the first frame update
    void Start()
    {
        //when its fired
        
        //rb.velocity = transform.right * speed;
        //meter a disparar em angulo

    }

    // Update is called once per frame
    void Update()
    {
        do_Trajectory(); //sempre a fazer trajetoria, mais facil user
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
        
    }

    void Shoot()
    {
        Instantiate(snowballPrefab, firepoint.position, firepoint.rotation);

        
        RaycastHit2D raycast = Physics2D.Raycast(firepointPosition, mousePosition,100);

        if (raycast.collider)
        {
            Enemy enemy = raycast.transform.GetComponent<Enemy>();

            if (enemy != null)
            {
                enemy.SufferDamage(damage);
            }
            Destroy(gameObject);
        }
        
        
    }

    void do_Trajectory()
    {
        
        Debug.DrawLine(firepointPosition, (mousePosition-firepointPosition)*100, Color.yellow);

    }


    //2 METODOS RAYCAST OU EM BAIXO, ESCOLHE TU


    void OnTriggerEnter2D(Collider2D collideObject)
    {
        //Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        //Vector2 firepointPosition = new Vector2(firepoint.position.x, firepoint.position.y);
        Debug.Log(collideObject.name);

        Enemy enemy = collideObject.GetComponent<Enemy>();
        if (enemy != null)
        {
            enemy.SufferDamage(damage);
        }
        Destroy(gameObject);
    }

}
