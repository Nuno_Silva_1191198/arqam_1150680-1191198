﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public struct Registered_Snowballs
{
    public Snowball real;
    public Snowball hidden;
}

public class ShootTrajectory : MonoBehaviour
{

    public static bool charching;

    public GameObject Snowball;
    public Transform referenceSnowBall;

    private Scene mainScene;
    private Scene physicsScene;

    public GameObject marker_trajectory;
    private List<GameObject> marker_trajectories = new List<GameObject>();
    private Dictionary<string, Registered_Snowballs> allSnowBalls =  new Dictionary<string, Registered_Snowballs>();

    public GameObject objectsToSpawn;

    public void Register_Snowballs(Snowball snowball)
    {
        if (allSnowBalls.ContainsKey(snowball.gameObject.name))
        {
            allSnowBalls[snowball.gameObject.name] = new Registered_Snowballs();

            var snowballs = allSnowBalls[snowball.gameObject.name];

            if(string.Compare(Snowball.gameObject.name,physicsScene.name) == 0)
            {
                snowballs.hidden = snowball;
            }
            else
            {
                snowballs.real = snowball;
            }

            allSnowBalls[snowball.gameObject.name] = snowballs;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Physics.autoSimulation = false;

        mainScene = SceneManager.GetActiveScene();
        physicsScene = SceneManager.CreateScene("physics-scene", new CreateSceneParameters(LocalPhysicsMode.Physics2D));

        //do
        PreparePhysicsScene();
    }


    void FixedUpdate()
    {
        if (Input.GetMouseButton(0))
            //do
            ShowTrajectory();
        mainScene.GetPhysicsScene().Simulate(Time.fixedDeltaTime);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PreparePhysicsScene()
    {
        SceneManager.SetActiveScene(physicsScene);
        
        GameObject g = GameObject.Instantiate(objectsToSpawn);
        g.transform.name = "ReferenceSnowball";  
        //g.GetComponent<Snowball>().isReference = true;
        Destroy(g.GetComponent<MeshRenderer>());

        SceneManager.SetActiveScene(mainScene);
    }

    public void CreateMovementMarkers()
    {
        foreach(var snowballType in allSnowBalls)
        {
            var snowball = snowballType.Value;
            Snowball hidden = snowball.hidden;

            //print(snowball.real.name);
            //print(snowball.hidden.name);

            GameObject g = GameObject.Instantiate(marker_trajectory, hidden.transform.position, Quaternion.identity);
            g.transform.localScale = new Vector2(0.3f, 0.3f);// SEE HERE
            marker_trajectories.Add(g);

        }
    }

    public void ShowTrajectory()
    {
        SyncArrow();
        ClearTrajectory();

        allSnowBalls["ReferenceSnowball"].hidden.transform.rotation = referenceSnowBall.transform.rotation;

        allSnowBalls["ReferenceSnowball"].hidden.GetComponent<Rigidbody>().velocity = referenceSnowBall.transform.TransformDirection(Vector2.up * 15f);
        allSnowBalls["ReferenceSnowball"].hidden.GetComponent<Rigidbody>().useGravity = true;

        int steps = (int)(2f / Time.fixedDeltaTime);

        for(int i = 0; i < steps; i++)
        {
            //GetPhysicsScene2D? OUR SME 2D
            physicsScene.GetPhysicsScene().Simulate(Time.fixedDeltaTime);
            CreateMovementMarkers();
        }

    }

    public void SyncArrow()
    {
        foreach (var snowballType in allSnowBalls)
        {
            var snowball = snowballType.Value;

            Snowball not_hidden = snowball.real;
            Snowball hidden = snowball.hidden;

            var rb = hidden.GetComponent<Rigidbody>();
            rb.velocity = Vector2.zero;
            rb.angularVelocity = Vector2.zero;

            hidden.transform.position = not_hidden.transform.position;
            hidden.transform.rotation = not_hidden.transform.rotation;

        }
    }

    public void ClearTrajectory()
    {
        foreach(var GO in marker_trajectories)
        {
            Destroy(GO);
        }
    }
}
